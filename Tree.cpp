#include "Tree.h"

Tree::Tree(int v)
{
	value = v;
	Right = new Tree();
	Left = new Tree();
}

Tree::Tree(int v, int rightValue, int leftValue)
{
	value = v;
	Right = new Tree(rightValue);
	Left = new Tree(leftValue);
}

Tree::Tree(const Tree& secTree)		// constructor of copying
{
	value = secTree.getValue();
	Right->setRight( secTree.getRightValue() );
	Left->setRight(secTree.getLeftValue());
}

Tree::~Tree()
{
	delete Right;
	delete Left;
}

Tree& Tree::operator=(const Tree& secTree)
{
	if (*this == secTree)
		return *this;
	delete Right;
	delete Left;
	this->setValue(secTree.getValue());
	Right = new Tree(secTree.getRightValue());
	Left = new Tree(secTree.getLeftValue());
	return *this;
}

bool operator==(const Tree& Tree1, const Tree& Tree2)
{
	if ( Tree1.getValue() == NULL && Tree2.getValue() == NULL )		// if all values equal
		return true;
	else if ( Tree1.getValue() == Tree2.getValue() )
			return Tree1.getRight() == Tree2.getRight() && Tree1.getLeft() == Tree2.getLeft();		// compare Right and Left branches with another
	else
		return false;
}

bool operator!=(const Tree& Tree1, const Tree& Tree2)
{
	if (Tree1 == Tree2)
		return false;
	else
		return true;
	
}

