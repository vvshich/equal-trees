#pragma once

#ifndef START
#define START
#define NULL 0

class Tree
{
public:
	Tree(int v);
	Tree(int v, int rightValue, int leftValue);
	Tree(const Tree&);						// constructor of copying
	Tree() { value = NULL; }				// default constructor
	~Tree();

	// getting values in branches
	int getValue() const { return value; }
	int getRightValue() const { return Right->getValue(); }
	int getLeftValue() const { return Left->getValue(); }

	// getting branches 
	Tree& getRight() const { return *Right; }
	Tree& getLeft() const { return *Left; }

	// seting values in branches
	void setValue(int v) { value = v; }
	void setRight(int v) { Right->setValue(v); }
	void setLeft(int v) { Left->setValue(v); }

	Tree& operator=(const Tree&);
    // recurse function to get all values from tree
	friend bool operator== (const Tree& Tree1, const Tree& Tree2);
	friend bool operator!= (const Tree& Tree1, const Tree& Tree2);

private:
	int value;
	//Branches
	Tree* Right;
	Tree* Left;
};

#endif // START
